const { lion, tiger } = require("./index");

describe("Zoo exercise tests", () => {
  it("Lion should roar", () => {
    const input = "I'm a lion";
    const expected = "I'm roar a roar lion roar";
    expect(lion.speak(input)).toEqual(expected);
  });
  it("Tiger should grr", () => {
    const input = "Lions suck";
    const expected = "Lions grrr suck grrr";
    expect(tiger.speak(input)).toEqual(expected);
  });
});
