class Animal {
  speak(what) {
    return what
      .split(" ")
      .map(el => el + ` ${this.sound}`)
      .join(" ");
  }
}

class Lion extends Animal {
  constructor() {
    super();
    this.sound = "roar";
  }
  speak(what) {
    return super.speak(what);
  }
}
class Tiger extends Animal {
  constructor() {
    super();
    this.sound = "grrr";
  }
  speak(what) {
    return super.speak(what);
  }
}

module.exports = {
  lion: new Lion(),
  tiger: new Tiger()
};
